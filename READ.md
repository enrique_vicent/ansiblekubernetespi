## using

quick test

```bash
ansible-inventory --list
ansible all -m ping
ansible -i hosts all -m command -a "whoami"
ansible localhost -m command -a "hostname"
ansible pi -m shell -a uptime
```

### vault

```bash
ansible-vault encrypt_string mysecret
ansible-playbook --vault-id @prompt ./playbooks/install_wget.yml
```

### operations

- `microk8s status`: Provides an overview of the MicroK8s state (running / not running) as well as the set of enabled addons
- `microk8s enable`: Enables an addon
- `microk8s disable`: Disables an addon
- `microk8s kubectl`: Interact with kubernetes
- `microk8s config`: Shows the kubernetes config file
- `microk8s istioctl`: Interact with the istio services; needs the istio addon to be enabled
- `microk8s inspect`: Performs a quick inspection of the MicroK8s intallation
- `microk8s reset`: Resets the infrastructure to a clean state
- `microk8s stop`: Stops all kubernetes services
- `microk8s start`: Starts MicroK8s after it is being stopped

## references

- Ansible
  - [Ansible Tutorial](https://www.golinuxcloud.com/ansible-tutorial/)
  - [Register Module Tutorial](https://linuxhint.com/ansible_register_module/)
  - [Ansible Manual](https://docs.ansible.com/ansible/latest/user_guide/index.html)
- Kubernetes
  - [microk8s Tutorials](https://microk8s.io/tutorials)
  - [microk8s Addons](https://microk8s.io/docs/addons)
  - [k8 ansible](https://github.com/mrlesmithjr/ansible-rpi-k8s-cluster)
  - [microk8s ansible role](https://github.com/istvano/ansible_role_microk8s)
- k3s
  - [used repo](https://github.com/k3s-io/k3s-ansible)
- This
  - [repo](https://bitbucket.org/enrique_vicent/ansiblekubernetespi)

hint [use docker](https://stackoverflow.com/questions/61119975/is-it-possible-to-set-microk8s-to-use-docker-engine-instead-of-containerd)

## Kubeconfig

To get access to your **Kubernetes** cluster just

```bash
scp debian@master_ip:/etc/rancher/k3s/k3s.yaml ~/.kube/config
```